import './App.css';
import './assets/styles/custom.scss';

import { MyRoutes } from './routes/routes';

export const App = () => {
  return <MyRoutes />;
};
