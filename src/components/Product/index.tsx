import './styles.css';

import { ReactComponent as ImageCar } from 'assets/img/car-card.svg';

export const Product = () => {
  return (
    <div className="product">
      <div className="car-card">
        <ImageCar />
      </div>
      <div className="description">
        <h3>Audi Supra TT</h3>
        <span>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate,
          nisi
        </span>
      </div>
      <button className="button-comprar">
        <span>COMPRAR</span>
      </button>
    </div>
  );
};
