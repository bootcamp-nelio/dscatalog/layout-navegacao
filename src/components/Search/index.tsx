import './styles.css';

export const Search = () => {
  return (
    <div className="card-container">
      <input
        placeholder="Digite sua busca"
        className="input-text form-control"
        type="text"
      />
      <button className="button">
        <span>BUSCAR</span>
      </button>
    </div>
  );
};
