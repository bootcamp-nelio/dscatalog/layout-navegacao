import './styles.css';

import { Search } from 'components/Search';
import { Product } from 'components/Product';

export const Catalog = () => {
  return (
    <>
      <div className="container">
        <div>
          <Search />
        </div>
        <div className=" row justify-content-between">
          <div className="col-sm-6 col-md-6 col-lg-4 col-xl-3">
            <Product />
          </div>
          <div className="col-sm-6 col-md-6 col-lg-4 col-xl-3">
            <Product />
          </div>
          <div className="col-sm-6 col-md-6 col-lg-4 col-xl-3">
            <Product />
          </div>
          <div className="col-sm-6 col-md-6 col-lg-4  col-xl-3">
            <Product />
          </div>
        </div>
      </div>
    </>
  );
};
